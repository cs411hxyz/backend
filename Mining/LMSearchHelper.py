import cPickle as pickle
import os
import nltk
from collections import defaultdict

class LMHelper:
    def __init__(self):
        self.D = defaultdict(int)
        self.total_words = 0
        self.total_tweets = 0
        self.stopwords = nltk.corpus.stopwords.words('english')
        self.stemmer = nltk.stem.porter.PorterStemmer()
        
    def add_tweet(self,tweet):
        new_tweet = self.process(tweet)
        for token in new_tweet:
            self.D[token] += new_tweet[token]
            self.total_words += new_tweet[token]
        self.total_tweets += 1
        if self.total_tweets % 50 == 0: # Save backup copy every 50 tweets
            pickle.dump(self,open('LMHelper.pkl','wb'))

    def process(self,tweet):
        new_tweet = defaultdict(int)
        for token in [t.lower() for t in nltk.word_tokenize(tweet)]:
            if token in self.stopwords:
                continue
            token = self.stemmer.stem(token)
            new_tweet[token] += 1
        return new_tweet
    
    def pwc(self,word):
        if word in self.D:
            return float(self.D[word])/self.total_words
        # If the word is not in the collection assign it an occurence of 1
        else:
            return 1.0/self.total_words

    def Save(self):
        pickle.dump(self,open('LMHelper.pkl','wb'))
    
if os.path.isfile('LMHelper.pkl'):
    C = pickle.load(open('LMHelper.pkl','rb'))
    print('LM Helper Loaded from Disk Successfully')
else:
    print('No saved LM Helper detected. Creating a new one.')
    C = LMHelper()

if __name__ == "__main__":
    print(C.D)
    C.add_tweet("This is hilarious!")
    print(C.D)
    print(C.pwc([key for key in C.process('hilarious')][0]))
    C.add_tweet("Let's see how good he is when I'm whipping pumpkin pies at 'em! ")
    print(C.D)
    print(C.pwc([key for key in C.process('hilarious')][0]))
    C.Save()