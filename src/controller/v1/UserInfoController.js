var restify = require('restify');
var CtrlBuilder = require('../Controller');
var HttpStatus = require('http-status-codes');
var loadDB = require('../../dbconn');

// Defines a prototypical class called UserInfoController
function UserInfoController() {}

CtrlBuilder.create(UserInfoController)
  .setPathPrefix('/me')
  .addGET('/', 'getCurrentUserInfo', function(request, response, next) {
    response.send(HttpStatus.OK, {userid: request.userid, username: request.username});
  })
  //**************** modify ******************
  .addPOST('/modify', 'modifyCurrentUserInfo', function(req, res, next) {
    var oldpass = req.params['old-pass'];
    var newpass = req.params['new-pass'];

    if(!oldpass || !newpass)
    {
      res.send(HttpStatus.BAD_REQUEST, {error: 'new and old password cannot be empty'});
      return;
    }

    var sql;
    loadDB().then(conn => {
      sql = conn;
      var sqlStr = sql.format(
        'UPDATE user SET password = ? WHERE name = ? AND password = ?',
        [newpass, req.username, oldpass]
      );
      return sql.query(sqlStr);
    }).then(result => {
      if(!result || !result.affectedRows)
        throw "Old password is incorrect";
      sql.end();
      res.send(HttpStatus.OK, {});
    }).catch(err => {
      res.send(HttpStatus.FORBIDDEN, {error: err.toString()});
      sql.destroy();
    });
  })
  //**************** DELETE ******************
  .addPOST('/delete', 'deleteCurrentUser', function(req, res, next) {
    var password = req.params.password;
    var username = req.params.username;

    if(!username || !password)
    {
      res.send(HttpStatus.BAD_REQUEST, {error: 'username and password cannot be empty'});
      return;
    }

    if(username != req.username)
    {
      res.send(HttpStatus.FORBIDDEN, {error: 'You cannot delete other users.'});
      return;
    }

    var sql;
    loadDB().then(conn => {
      sql = conn;
      var sqlStr = sql.format(
        'DELETE FROM user WHERE name = ? AND password = ?',
        [username, password]
      );
      return sql.query(sqlStr);
    }).then(result => {
      if(!result || !result.affectedRows)
        throw "Old password is incorrect";
      sql.end();
      res.send(HttpStatus.OK, {});
    }).catch(err => {
      res.send(HttpStatus.FORBIDDEN, {error: err.toString()});
      sql.destroy();
    });
  });


module.exports = UserInfoController;
