var restify = require('restify');
var CtrlBuilder = require('../Controller');
var HttpStatus = require('http-status-codes');
var loadDB = require('../../dbconn');
var http = require('request');

// Defines a prototypical class called SearchController
function SearchController() {}

CtrlBuilder.create(SearchController)
  .setPathPrefix('/search')
  .addPOST('/', 'TextMiningSearch', function(req, res, next) {
    if(!req.params.query)
      req.params.query = '';
    if(!req.params.category)
      req.params.category = '';
    if(!req.params.time)
      req.params.time = '';
    if(!req.params.feedback)
      req.params.feedback = 'False';
    if(!req.params.feedback_tweets)
      req.params.feedback_tweets = '[]';
    if(!req.params.min_num_followers)
      req.params.min_num_followers = '0';

    http.post({url: 'http://localhost:8899/search', json: true, body: req.params}, (_err, _res, _body) => {
        if(_err) {
          res.send(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to contact search engine");
          console.error("Connection to Search Engine failed");
          console.error(_err);
        }
        else res.send(_res.statusCode, _body);
      });
  })
  .addGET('/basic', 'basicSearch', function(request, response, next) {
    var keyword = request.params.keyword;
    if(!keyword)
    {
      response.send(HttpStatus.BAD_REQUEST, {error: 'Please enter a keyword'});
      return;
    }
    var sql;
    loadDB().then(conn => {
      sql = conn;
      var qry = sql.format(
        'SELECT tweets.content, author.name, author.numfollowers ' +
        'FROM tweets, author, writtenby                          ' +
        'WHERE tweets.tid = writtenby.tid                        ' +
        'AND author.aid = writtenby.aid                          ' +
        'AND tweets.content LIKE ? COLLATE utf8_general_ci      ;' ,
        ['%' + keyword + '%']);
      return sql.query(qry);
    }).then(rows => {
      var answer = [];
      for(var i = 0; i < rows.length; i++)
      {
        var item = {};
        item.text = rows[i].content;
        item.author = rows[i].name;
        item.authfollow = rows[i].numfollowers;
        answer[i] = item;
      }
      response.send(HttpStatus.OK, answer);
    }).then(() => sql.destroy())
    .catch(err => {
      sql.destroy();
      response.send(HttpStatus.NOT_FOUND, err);
    });
  });

module.exports = SearchController;
