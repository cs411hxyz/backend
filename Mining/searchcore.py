mu = 20.0 # Dirichlet Prior Parameter
epsi = 0.1 # Additive Smoothing Parameter for Feedback Method
Lambda = 0.01 # Word Penalization Parameter in DMM
num_top_words = 20 # Number of top words to consider in query expansion
alpha = 0.5 # The query interpolation coefficient
categories = ['News', 'Sports', 'Technology', 'Entertainment', 'Science']

import nltk
import inspect, os, sys
from LMSearchHelper import C as SearchHelper
from collections import defaultdict
from math import log
from databaseconnect import database_connect


def connect():
    global db
    db = database_connect()

def extract_from_db(query, category, time_range, num_followers, exhaustive=False):
    ### IMPLEMENT TIME RANGE SEARCH LATER ###
    if exhaustive == False:
        # Extract tweets that exactly match the query words
        keywords = [t for t in nltk.word_tokenize(query)]
        if query == '':
            keywords = ['']
    else:
        # Extract all the tweets. Let the search engine completely handle the search
        keywords = ['']
    if category != '':
        #results = db.fetch({'table':'tweets','category':category,'keywords':keywords, 'id':'','url':'', 'timestamp':time_range}) ### 'timestamp':'',
        # select tweets where author has >= num of followers:
        ret1={'content':'','category':category,'timestamp':time_range,'id':'','keywords':keywords}
        ret2={'id':'','numfollowers':num_followers,'email':'',"name":''}
        join={"tweets":"author","author":"name"} # common attributes, tb:attri
        results=db.select_from_join("tweets",ret1,"author",ret2,join)

    else:
        results = []
        for category in categories:
            #results += db.fetch({'table':'tweets', 'category':category,'keywords':keywords, 'id':'','url':'', 'timestamp':time_range}) ### 'timestamp':'',
            ret1={'content':'','category':category,'timestamp':time_range,'id':'','keywords':keywords,'url':''}
            ret2={'id':'','numfollowers':num_followers,'email':'',"name":''}
            join={"tweets":"author","author":"name"} # common attributes, tb:attri
            results+=db.select_from_join("tweets",ret1,"author",ret2,join)
    

    # Duplicate Elimination
    
    tids = set()
    final_results = []
    for result in results:
        if result["tid"] not in tids:
            tids.add(result["tid"])
            final_results.append(result)
    return final_results
    
    #return results

def search(tweets, query,feedback=False):
    '''
    Takes a list of tweets extracted from the database and ranks them
    using the KL-Divergence retrieval function with Dirichlet prior smoothing
    '''
    if feedback == False:
        q = SearchHelper.process(query)
        lenq = 0
        for word in q:
            lenq += q[word]
        for word in q:
            q[word] = q[word]/float(lenq) # Maximum Likelihood estimate

    else:
        q = query
    # Else if feedback, let the feedback method handle the query and interpolation
    
    ranked_list = []
    for tweet in tweets:
        # Process each tweet and assign it a score
        tweet_processed = SearchHelper.process(tweet['content'])
        lenD = 0
        for word in tweet_processed:
            lenD += tweet_processed[word]
        # Smooth the probability using Dirichlet Prior
        if (set(tweet_processed.keys()).intersection(set(q.keys()))!=set()):
            for word in q: # Because we only care about the intersection
                tweet_processed[word] = Smoothing(word,tweet_processed[word],lenD)
            tweet['score'] = KLDivRetrievalFunction(q,tweet_processed,lenD)
            # Change time to str (for JSON compatibility purposes)
            tweet['timestamp'] = tweet['timestamp'].ctime()
            ranked_list.append(tweet)
        
    ranked_list = sorted(ranked_list, key = lambda x : x['score'], reverse=True)[:200] # Top 200 tweets
    return ranked_list

def KLDivRetrievalFunction(PWQ,PWD,lenD):
    score = 0
    for word in PWQ:
        score = score + float(PWQ[word])*log((float(PWD[word])*(lenD+mu))/(mu*SearchHelper.pwc(word)))
    score = score + log(mu/(mu+lenD))
    return score

def Smoothing(word,count,lenD):
    return (count+mu*SearchHelper.pwc(word))/(lenD+mu)

def mean(L):
    return sum(L)/float(len(L))

def search_feedback(tweets, query, relevant_tweets):
    # In this case relevant_tweets is a list containing the text of the relevant relevant_tweets
    q = SearchHelper.process(query)
    lenq = 0
    for word in q:
        lenq += q[word]
    for word in q:
        q[word] = q[word]/float(lenq) # Maximum Likelihood estimate

    lengths = [0 for i in range(len(relevant_tweets))]
    feedback_set = set()
    # Convert the relevant relevant_tweets to language models    
    for i in range(len(relevant_tweets)):
        relevant_tweets[i] = SearchHelper.process(relevant_tweets[i])
        for word in relevant_tweets[i]:
            lengths[i] += 1
            feedback_set.add(word)   
    
    for word in relevant_tweets[i]:
        relevant_tweets[i][word] = (relevant_tweets[i][word]+epsi)/(lengths[i] + epsi*len(feedback_set))
    # Now the relevant relevant_tweets are smoothed LMs
    # Next, calculate the probability of each word using DMM
    word_score = {}
    total_score = 0
    for word in feedback_set:
        word_score[word] = (mean([tweet[word] for tweet in relevant_tweets])**(1/(1-Lambda)))/((SearchHelper.pwc(word))**(Lambda/(1-Lambda)))
        total_score += word_score[word]
        
    for word in feedback_set:
        word_score[word] = word_score[word]/total_score
        
    # Sort the words by probability and take the top 20 only
    top_words = sorted(word_score.items(), key = lambda x : x[1], reverse=True)[:min(num_top_words,len(word_score))]
    
    # Renormalize the probabilities (due to truncation)
    top_words_dict = defaultdict(float)
    total_score = 0
    for word in top_words:
        total_score += word[1] # word[0] is the word's text and word[1] is the score
    for word in top_words:
        top_words_dict[word[0]] = word[1]/total_score

    # Add the feedback LM to the original query's LM
    all_words = set([word for word in top_words_dict]).union(set([word for word in q]))
    interpolated_query = defaultdict(float)
    for word in all_words:
        interpolated_query[word] = (1-alpha)*q[word] + alpha*top_words_dict[word]
    print interpolated_query
    return search(tweets, interpolated_query, True)
    
    
def search_interface(query, category, time_range, feedback=False, feedback_tweets='', num_followers=1, exhaustive=False):
    if feedback == True:
        exhaustive = True
    tweets = extract_from_db(query, category, time_range, num_followers, exhaustive)
    # Search by Category
    if (query == '' and feedback==False):
        # Convert the time in each tweet to JSON serializable format
        for tweet in tweets:
            tweet['timestamp'] = tweet['timestamp'].ctime()
        
        return tweets
    # Search by query and category
    if feedback:
        return search_feedback(tweets, query, feedback_tweets)
    else:
        return search(tweets, query,feedback=False)



#x = extract_from_db('news iphone apple','News',0,True)
#for row in x:
#    print row['content']
#r = search_feedback(x,'news iphone apple')
#r = search_feedback(x,'christmas buy',["Looking for a new computer for Christmas? Here's our pick of the best laptops and desktops",'iphone 6 best product',"The top Black Friday deals you can get at Best Buy"])
'''
query = 'iphone' # christmas buy
category = 'Technology'
feedback = False
feedback_tweets = ["Looking for a new computer for Christmas? Here's our pick of the best laptops and desktops",'iphone 6 best product',"The top Black Friday deals you can get at Best Buy"]
exhaustive = False
r = search_interface(query, category, 0, feedback, feedback_tweets, exhaustive)
print('######################################################################')
for row in r:
    print row['content'] # str(row['score'])+'    '+row['content']
'''