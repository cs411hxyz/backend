var restify = require('restify');
var CtrlBuilder = require('../Controller');
var HttpStatus = require('http-status-codes');

// Defines a prototypical class called TestController
function TestController() {};

CtrlBuilder.create(TestController)
  .setPathPrefix('/test')
  .addGET('/', 'getTestRootPage', function(request, response, next) {
    response.send(HttpStatus.OK, {"type": "text", "data": "Test OK!"});
    next();
  });

module.exports = TestController;
