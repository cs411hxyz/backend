#!/usr/bin/env python

import os, sys
import subprocess, threading
import random, time
import platform
import inspect
mypydir =os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.append(mypydir)
import mysql.connector
from mysql.connector import errors
from mysql.connector import Error
from mysql.connector.pooling import MySQLConnectionPool
import datetime
from copy import deepcopy

from hostip import host2ip, ip2host, host2userip, nid2ip
import logging
import consistenthash


TotalNodeNum=35
NumOfCopies=3 # 1,2... one means no backup
POOLSIZE=25 # mysql conn max num
RingNodeNum=8 # nodes per ring
PoolExhaustSleep=2 # when sql conn get fails
CheckAliveIntv=3 # heartbeat

DEBUG_PRINT=4
INFO_PRINT=3
WARN_PRINT=2
ERROR_PRINT=1
PRINTLEVEL=1

dbname='cs411'

config = {
  'user': 'root',
  'password': 'edison',
  'host': '',
  'database': dbname,
}
poolconfig = deepcopy(config)
nodbconfig={'user': 'root','password': 'edison','host': '',}

class Database:

	def __init__(self):
		self.nid2connPool={}
		self.name2ring={}
		self.deadlist=[]
		self.nid2thread={}
		#consistenthash.Ring() # default 256 space, 2 virtual nodes.

	def th_exec_sql(self,nid,cmd,value,result=[],read=True,write=False,newConn=False):
		cursor=None
		cnx=None
		if newConn: # exec on every node using new connection
			tmpconfig=deepcopy(config)
			#for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]
			tmpconfig['host']=ip
			try:
				if PRINTLEVEL>=DEBUG_PRINT: print `nid`+" exec "+cmd+' '+threading.currentThread().getName()
				cnx=mysql.connector.connect(**tmpconfig)
				cursor=cnx.cursor(dictionary=True)
				if read:
					cursor.execute(cmd)
					row = cursor.fetchone()# fetchone(),  fetchmany() or  fetchall()
					while row:
						result.append(row) # list append dict
						row = cursor.fetchone()
				if write:
					cursor.execute(cmd,value) # here is the difference
					cnx.commit()
				cursor.close()
				cnx.close()
			except:
				pass
		else:
			done=False
			sleept=PoolExhaustSleep
			while not done:
				try:
					if PRINTLEVEL>=DEBUG_PRINT: print `nid`+" exec "+cmd+' '+threading.currentThread().getName()
					cnx= self.nid2connPool[nid].get_connection()
					cursor=cnx.cursor(dictionary=True)
					if read:
						cursor.execute(cmd)
						row = cursor.fetchone()# fetchone(),  fetchmany() or  fetchall()
						while row:
							result.append(row) # list append dict
							row = cursor.fetchone()
					if write:
						cursor.execute(cmd,value)
						cnx.commit()
					done=True
				except errors.PoolError as error:
					if PRINTLEVEL>=WARN_PRINT: print 'nid %d %s pool exhausted!'%(nid,cmd)
					if PRINTLEVEL>=ERROR_PRINT: print(error)
					time.sleep(sleept)
					sleept*=1.2
				except Exception, error:
					if PRINTLEVEL>=ERROR_PRINT: print 'nid %d exec %s error!'%(nid,cmd)
					if PRINTLEVEL>=ERROR_PRINT: print(error)
					done=True
				finally:
					if cursor: cursor.close()
					if cnx: cnx.close()
		th=threading.currentThread().getName()
		if th in self.nid2thread[nid]:
			self.nid2thread[nid].remove(th)
			print th+" finished on "+`nid`
			
	def th_exec_raw(self,nid,cmd,result=[],read=True,write=False,newConn=False):
		cursor=None
		cnx=None
		if not newConn and not nid in self.nid2connPool.keys(): # need new conn
			newConn=True
		if newConn: # exec on every node using new connection
			tmpconfig=deepcopy(config)
			#for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]
			tmpconfig['host']=ip
			try:
				if PRINTLEVEL>=DEBUG_PRINT: print `nid`+" exec "+cmd+' '+threading.currentThread().getName()
				cnx=mysql.connector.connect(**tmpconfig)
				cursor=cnx.cursor(dictionary=True)
				cursor.execute(cmd)
				if read:
					row = cursor.fetchone()# fetchone(),  fetchmany() or  fetchall()
					while row:
						result.append(row) # list append dict
						row = cursor.fetchone()
				if write:
					cnx.commit()
				cursor.close()
				cnx.close()
			except:
				pass
		else:
			done=False
			sleept=PoolExhaustSleep
			while not done:
				try:
					if PRINTLEVEL>=DEBUG_PRINT: print `nid`+" exec "+cmd+' '+threading.currentThread().getName()
					cnx= self.nid2connPool[nid].get_connection()
					cursor=cnx.cursor(dictionary=True)
					cursor.execute(cmd)
					if read:
						row = cursor.fetchone()# fetchone(),  fetchmany() or  fetchall()
						while row:
							result.append(row) # list append dict
							row = cursor.fetchone()
					if write:
						cnx.commit()
					done=True
				except errors.PoolError as error:
					if PRINTLEVEL>=WARN_PRINT: print 'nid %d %s pool exhausted!'%(nid,cmd)
					if PRINTLEVEL>=ERROR_PRINT: print(error)
					time.sleep(sleept)
					sleept*=1.2
				except Exception, error:
					if PRINTLEVEL>=ERROR_PRINT: print 'nid %d exec %s error!'%(nid,cmd)
					if PRINTLEVEL>=ERROR_PRINT: print(error)
					done=True
					if cnx is None: # nid is dead!
						if PRINTLEVEL>=INFO_PRINT: print 'nid %d dead!',nid
						for name,ring in self.name2ring.items():
							if nid in ring.nid2node.keys():
								if not nid in ring.extradict['dead']: # could append else where
									ring.extradict['dead'].append(nid) # notify
				finally:
					if cursor: cursor.close()
					if cnx: cnx.close()

	def find_max_attri_val(self,tbname,attri,name=''):
		qry="SELECT MAX("+attri+") FROM "+tbname
		if name=='':
			nidlist=nid2ip.keys()
			newconn=True
		else:
			nidlist=self.name2ring[name].nid2node.keys()
			newconn=False
		result=[]
		thqueue=[]
		for nid in nidlist:
			if nid in self.deadlist:
				continue
			t = threading.Thread(target=self.th_exec_raw,args=(nid,qry,result,True,False,newconn))
			t.setDaemon(True)
			t.start()
			thqueue.append(t)
		if PRINTLEVEL>=DEBUG_PRINT: print 'finding max val %s %s %s'%(name,tbname,attri)
		for th in thqueue:
			th.join()
		maxval=0
		for dic in result:
			if maxval<dic.values()[0]:
				maxval=dic.values()[0]
		return maxval

# SELECT table1111.id,table111.aval1,table113.cval1  
# FROM table111  
# INNER JOIN table113  
# ON table111.id=table113.id;
	def select_from_join(self,tbname1,ret1,tbname2,ret2,joinon):
		tb1=str(tbname1)
		tb2=str(tbname2)
		qry1="SELECT DISTINCT "
		qry2=" FROM "+tb1+" INNER JOIN "+tb2+" "
		qry3=" ON "
		qry4=" WHERE "+tb1+".baknum=0 AND "+tb2+".baknum=0"
		for tb,val in joinon.items():
			qry3=qry3+tb+"."+val+"="
		qry3=qry3.rstrip('=')
		paramdict=ret1
		tb=tb1
		for i in range(2):
			if 'id' in paramdict.keys():
				qry1=qry1+tb+"."+tb[0]+"id,"
				if str(paramdict['id'])!='':	# specified id
					qry4=qry4+" AND "+tb+"."+tb[0]+"id="+str(paramdict['id'])
			paramdict=ret2
			tb=tb2
		paramdict=ret1
		tb=tb1
		for i in range(2):
			if 'timestamp' in paramdict.keys():
				qry1=qry1+tb+".timestamp,"
				timestamp=paramdict['timestamp']
				if timestamp!='':				# specified timestamp
					qry4=qry4+" AND "+tb+".timestamp>='"+str(timestamp)+"'"
			paramdict=ret2
			tb=tb2
		paramdict=ret1
		tb=tb1
		for i in range(2):
			if 'numfollowers' in paramdict.keys():
				qry1=qry1+tb+".numfollowers,"
				numfollowers=paramdict['numfollowers']
				if numfollowers!='':				# specified numfollowers
					qry4=qry4+" AND "+tb+".numfollowers>="+str(numfollowers)
			paramdict=ret2
			tb=tb2
		paramdict=ret1
		tb=tb1
		for i in range(2):
			if 'keywords' in paramdict.keys():
				qry4=qry4+" AND ( "
				for word in paramdict['keywords']:
					like='%'+str(word)+'%'
					qry4=qry4+tb+".content LIKE '"+like+"' OR "
				qry4=qry4.rstrip('OR ')+")"
			paramdict=ret2
			tb=tb2
		paramdict=ret1
		tb=tb1
		for i in range(2):
			for attri in paramdict.keys():
				if not attri in ['timestamp','id','keywords','numfollowers']: 
					val=paramdict[attri]
					qry1=qry1+tb+"."+attri+","
					if val!='':				# specified val
						qry4=qry4+" AND "+tb+"."+attri+"='"+str(val)+"'"
			paramdict=ret2
			tb=tb2
		category=''
		if 'category' in ret1.keys() and ret1['category']!='':
			category=ret1['category']
		if 'category' in ret2.keys() and ret2['category']!='':
			category=ret2['category']
		if category!='':
			ring=self.name2ring[category]
			nlist=ring.nid2node.keys()
		else:
			nlist=nid2ip.keys()
		cmd=qry1.rstrip(",")+qry2+qry3+qry4+' GROUP BY tid'
		result=[]
		thqueue=[]
		for nid in nlist:
			t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,result,True,False))
			t.setDaemon(True)
			t.start()
			thqueue.append(t)
		for th in thqueue:
			th.join()
		return result

	def insert(self,paramdict):
		tbname=paramdict['table']
		qry1='INSERT INTO '+tbname+'('
		qry2=') VALUES ('
		qry3=')'
		nextval=0
		if tbname=='tweets': # tid, timestamp, url, content, category
			category=paramdict['category']
			content=paramdict['content']
			qry1=qry1+"content"
			qry2=qry2+"'"+content.replace('"', '\\"').replace("'", "\\'")+"'"  #  escape '\''
			if 'id' in paramdict.keys() and str(paramdict['id'])!='':
				tid=paramdict['id']
			else:
				maxval=self.find_max_attri_val(tbname,'tid')
				tid=maxval+1   # use max tid as new tid.
				if PRINTLEVEL>=DEBUG_PRINT: print 'tid use max val: %d'%tid
			qry1=qry1+",tid"
			qry2=qry2+","+str(tid)
			nextval=tid+1
			for attri in paramdict.keys():
				if not attri in ['table','id','content']: # category as attri here
					val=paramdict[attri]
					qry1=qry1+","+attri
					qry2=qry2+",'"+str(val)+"'"
			ring=self.name2ring[category]
			pos=ring.key2pos(tid) # primary copy position in ring
			avoid=[]
			for bak in range(NumOfCopies):
				tmpqry1=qry1+",baknum"
				tmpqry2=qry2+","+str(bak)
				cmd=tmpqry1+tmpqry2+qry3
				if PRINTLEVEL>=DEBUG_PRINT: print 'insert tid '+str(tid)+' at pos '+str(pos)
				nid=ring.pos2node[pos].nid
				t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,[],False,True))
				t.setDaemon(True)
				t.start()
				pos=ring.find_succ_diffnode(pos,avoid)

		elif tbname=='user': # insert uid, name, password, email, token
			category=paramdict['category'] # userinfo
			if 'id' in paramdict.keys() and str(paramdict['id'])!='':
				uid=paramdict['id']
			else:
				maxval=self.find_max_attri_val(tbname,'uid')
				uid=maxval+1   # use max uid as new uid.
				if PRINTLEVEL>=DEBUG_PRINT: print 'uid use max val: %d'%uid
			qry1=qry1+"uid"
			qry2=qry2+str(uid)
			nextval=uid+1
			for attri in paramdict.keys():
				if not attri in ['id','table','category']:
					val=str(paramdict[attri])
					qry1=qry1+","+attri
					qry2=qry2+",'"+(val)+"'"
			ring=self.name2ring[category]
			pos=ring.key2pos(uid) # primary copy position in ring
			avoid=[]
			for bak in range(NumOfCopies):# maybe user should be on every node? join?
				tmpqry1=qry1+",baknum"
				tmpqry2=qry2+","+str(bak)
				cmd=tmpqry1+tmpqry2+qry3
				if PRINTLEVEL>=DEBUG_PRINT: print 'insert uid '+str(uid)+' at pos '+str(pos)
				nid=ring.pos2node[pos].nid
				t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,[],False,True))
				t.setDaemon(True)
				t.start()
				pos=ring.find_succ_diffnode(pos,avoid)

		elif tbname=='author': # insert aid, name, numfollowers, email, username
			#category=paramdict['category'] # userinfo
			if 'id' in paramdict.keys() and str(paramdict['id'])!='':
				aid=paramdict['id']
			else:
				maxval=self.find_max_attri_val(tbname,'aid')
				aid=maxval+1   # use max aid as new aid.
				if PRINTLEVEL>=DEBUG_PRINT: print 'aid use max val: %d'%aid
			uid=aid
			qry1=qry1+"aid"
			qry2=qry2+str(uid)
			nextval=uid+1
			if 'numfollowers' in paramdict.keys() and str(paramdict['numfollowers'])!='':
				numfollowers=paramdict['numfollowers']
				qry1=qry1+",numfollowers"
				qry2=qry2+","+str(numfollowers)
			for attri in paramdict.keys():
				if not attri in ['id','table','category','numfollowers']:
					val=str(paramdict[attri])
					qry1=qry1+","+attri
					qry2=qry2+",'"+(val)+"'"
			nlist=nid2ip.keys() # author no all nodes!
			for nid in nlist:
				tmpqry1=qry1+",baknum"
				tmpqry2=qry2+","+str(0)
				cmd=tmpqry1+tmpqry2+qry3
				t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,[],False,True))
				t.setDaemon(True)
				t.start()
			# ring=self.name2ring[category]
			# pos=ring.key2pos(uid)
			# avoid=[]
			# for bak in range(NumOfCopies):# maybe author should be on every node? sql join?
			# 	tmpqry1=qry1+",baknum"
			# 	tmpqry2=qry2+","+str(bak)
			# 	cmd=tmpqry1+tmpqry2+qry3
			# 	if PRINTLEVEL>=DEBUG_PRINT: print 'insert aid '+str(uid)+' at pos '+str(pos)
			# 	nid=ring.pos2node[pos].nid
			# 	t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,[],False,True))
			# 	t.setDaemon(True)
			# 	t.start()
			# 	pos=ring.find_succ_diffnode(pos,avoid)
				
		return nextval # suggest next id for app to use when insert.

	def fetch(self,paramdict):
		tbname=paramdict['table']
		qry1="SELECT "
		qry2=" FROM "+tbname+" WHERE baknum=0 "
		result=[]
		thqueue=[]
		if tbname=='tweets': # tid, timestamp, url, content
			
			qry1=qry1+"content"
			tid=-1
			if 'id' in paramdict.keys():
				qry1=qry1+",tid"
				if str(paramdict['id'])!='':	# specified id
					tid=int(paramdict['id'])
					qry2=qry2+" AND tid="+str(tid)
			if 'baknum' in paramdict.keys():
				qry1=qry1+",baknum" # just give, no specified
			if 'timestamp' in paramdict.keys():
				qry1=qry1+",timestamp"
				timestamp=paramdict['timestamp']
				if timestamp!='':				# specified timestamp
					qry2=qry2+" AND timestamp>='"+timestamp+"'"
			if 'url' in paramdict.keys():
				qry1=qry1+",url"
				url=paramdict['url']
				if url!='':				# specified url
					qry2=qry2+" AND url='"+str(url)+"'"
			if 'author' in paramdict.keys():
				qry1=qry1+",author"
				author=paramdict['author']
				if author!='':				# specified author
					qry2=qry2+" AND author='"+str(author)+"'"
			if 'category' in paramdict.keys():
				qry1=qry1+",category"
				category=paramdict['category']
				if category!='':				# specified category
					qry2=qry2+" AND category='"+str(category)+"'"
			if 'keywords' in paramdict.keys():
				qry2=qry2+" AND ( "
				for word in paramdict['keywords']:
					like='%'+word+'%'
					qry2=qry2+"content LIKE '"+like+"' OR "
				qry2=qry2.rstrip('OR ')+")"
			cmd=qry1+qry2
			if 'category' in paramdict.keys() and paramdict['category']!='':
				category=paramdict['category']
				ring=self.name2ring[category]
				nlist=ring.nid2node.keys()
			else:
				nlist=nid2ip.keys()
			for nid in nlist:
				if nid in self.deadlist:
					if tid!=-1 and ring.key2nid(tid)==nid: # ask two successor for baknum 1,2
						for dpos in ring.nid2node[nid].poslist:
							sucpos=dpos
							for copy in range(1,NumOfCopies):
								sucpos= find_succ_diffnode(sucpos)
								sucid=ring.pos2node[sucpos].nid
								cmd2=cmd.replace("WHERE baknum=0","WHERE baknum="+`copy`)
								if sucid in self.deadlist:
									continue
								t = threading.Thread(target=self.th_exec_raw,args=(sucid,cmd2,result,True,False))
								t.setDaemon(True)
								t.start()
								thqueue.append(t)
					elif tid==-1: # no tid specify but nodes die
						cmd=cmd.replace("WHERE baknum=0","WHERE baknum>=0")# just change cmd for all...
				else: # nid not dead, alive!
					t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,result,True,False))
					t.setDaemon(True)
					t.start()
					thqueue.append(t)

		elif tbname=='user': # fetch uid, name, email, password
			category=paramdict['category']
			qry1=qry1+"uid"
			if 'id' in paramdict.keys(): # uid has to be there, no matter what
				uid=str(paramdict['id'])
				if uid!='':
					qry2=qry2+" AND uid="+uid
			for attri in paramdict.keys():
				if attri not in ['table','category','id']: # already processed
					qry1=qry1+","+attri
					val=str(paramdict[attri])
					if val!='':				# specified val
						qry2=qry2+" AND "+attri+"='"+val+"'"
			cmd=qry1+qry2
			ring=self.name2ring[category]
			for nid in ring.nid2node.keys():
				t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,result,True,False))
				t.setDaemon(True)
				t.start()
				thqueue.append(t)

		elif tbname=='author': # fetch aid, name, numfollowers, username,email
			#category=paramdict['category']
			qry1=qry1+"aid"
			if 'id' in paramdict.keys(): # uid has to be there, no matter what
				uid=str(paramdict['id'])
				if uid!='':
					qry2=qry2+" AND aid="+uid
			if 'numfollowers' in paramdict.keys():
				qry1=qry1+",numfollowers"
				val=str(paramdict['numfollowers'])
				if val!='':				 # specified numfollowers
					qry2=qry2+" AND numfollowers="+val
			for attri in paramdict.keys():
				if attri not in ['table','category','id','numfollowers']: # already processed
					qry1=qry1+","+attri
					val=str(paramdict[attri])
					if val!='':				# specified val
						qry2=qry2+" AND "+attri+"='"+val+"'"
			cmd=qry1+qry2
			#ring=self.name2ring[category]
			nlist=nid2ip.keys()
			for nid in nlist:
				if nid in self.deadlist:
					continue
				t = threading.Thread(target=self.th_exec_raw,args=(nid,cmd,result,True,False))
				t.setDaemon(True)
				t.start()
				thqueue.append(t)
				break   # only one node returns
		#return result # return huge at once? better yield.
		#but all thread are finished before result get appended, async list... still empty
		#result when th.is_alive == False...
		for th in thqueue:
			th.join()
		return result
		#time.sleep(1)

		# num=0
		# for i in result:
		# 	yield i
		# 	num+=1
		# 	sttime=time.time()
		# 	while num>=len(result) and time.time()<sttime+5: # sleep so that for elem in iterate also waits.
		# 		finish=True
		# 		for th in thqueue:
		# 			if th.is_alive():
		# 				print 'th not finishing\n\n\n\n'
		# 				finish=False
		# 				break
		# 		time.sleep(0.2)
					#break

	def createCategory(self,category='notknown',myRingNodeNum=RingNodeNum):
		if category in init_name2nids.keys():
			self.createName2ring(category,init_name2nids[category],myRingNodeNum)
		else:
			nidlist=[]
			startn=consistenthash.get_hash(category)%TotalNodeNum
			while len(nidlist)<myRingNodeNum: # just store in consecutive nodes.
				nidlist.append(startn)
				startn=(startn+1)%TotalNodeNum
			self.createName2ring(category,nidlist,myRingNodeNum)

	def createName2ring(self,name='notknown',ringnidlist=[],myRingNodeNum=RingNodeNum):
		if not name in self.name2ring.keys():
			self.name2ring[name]=consistenthash.Ring()
			thqueue=[]
			count=0
			for nid in (ringnidlist):
				if count>=myRingNodeNum: # num of nodes per ring
					break
				t = threading.Thread(target=self.connectNode,args=(nid,))
				t.setDaemon(True) # first add to conn pool
				t.start()
				thqueue.append(t)
				count+=1
			for th in thqueue:
				if PRINTLEVEL>=DEBUG_PRINT-1: print "joining "+th.getName()
				th.join() # after 5s deem node dead.
			for nid in sorted(ringnidlist[0:count]):  # must ascending nid order!
				if self.name2ring[name].add_node(nid)==0:# then add to ring
					if PRINTLEVEL>=ERROR_PRINT: print 'ring %s add nid %d fail!'%(name,nid)

			if PRINTLEVEL>=DEBUG_PRINT: print self.name2ring[name].sortedpos
			if PRINTLEVEL>=DEBUG_PRINT+1: print self.name2ring[name].pos2node
			if PRINTLEVEL>=DEBUG_PRINT+1: print self.name2ring[name].nid2node
			return 1
		if PRINTLEVEL>=INFO_PRINT: print name+' ring already created!'
		return 2

	def deleteName2ring(self,name):
		del self.name2ring[name]

	def connectNode(self,nid): # we have 35 small nodes.
		if not nid in self.nid2connPool.keys():
			ip=nid2ip[nid]
			tmpconfig=deepcopy(poolconfig)
			tmpconfig['host']=ip
			try:
				if PRINTLEVEL>=INFO_PRINT: print "MySQLConnectionPool connectNode "+`nid`
				cnxpool=MySQLConnectionPool(pool_name=str(nid),pool_size=POOLSIZE,**tmpconfig)
				if PRINTLEVEL>=INFO_PRINT: print `nid`+" MySQLConnectionPool done! "+threading.currentThread().getName()
				self.nid2connPool[nid]=cnxpool
				return 1
			except Exception, e:
				if PRINTLEVEL>=ERROR_PRINT: print nid, e
			return 0
		return 2

	def th_modify_table(self,nid,tb,opcode):
		if nid in self.deadlist:
			return
		cursor=None
		try:
			cnx= self.nid2connPool[nid].get_connection()
			cursor=cnx.cursor(dictionary=True)
			if opcode=='create':
				cmd=TABLES[tb]
			elif opcode=='drop':
				cmd="DROP TABLE IF EXISTS "+tb
			cursor.execute(cmd)
			if PRINTLEVEL>=DEBUG_PRINT: print 'node '+`nid`+' create_table '+tb+' done!'
		except Exception, e:
			if PRINTLEVEL>=ERROR_PRINT: print(e)
		finally:
			if cursor:
				cursor.close()
			cnx.close()

	def modify_table(self,opcode,tbname='',name=''):
		thqueue=[]
		if name=='':
			for tb in TABLES.keys():
				if tbname=='' or tbname==tb: # create specified table, or all tables
					for nid in self.nid2connPool.keys():
						t = threading.Thread(target=self.th_modify_table,args=(nid,tb,opcode))
						t.setDaemon(True)
						t.start()
						thqueue.append(t)
					for na in self.name2ring.keys():
						if opcode=='create':
							if not tb in self.name2ring[na].extralist:
								self.name2ring[na].extralist.append(tb)
						elif opcode=='drop':
							if tb in self.name2ring[na].extralist:
								self.name2ring[na].extralist.remove(tb)
		else:
			for tb in TABLES.keys():
				if tbname=='' or tbname==tb: # create specified table, or all tables
					for nid in self.name2ring[name].nid2node.keys():
						t = threading.Thread(target=self.th_modify_table,args=(nid,tb,opcode))
						t.setDaemon(True)
						t.start()
						thqueue.append(t)
					if opcode=='create':
						if not tb in self.name2ring[name].extralist:
							self.name2ring[name].extralist.append(tb)
					elif opcode=='drop':
						if tb in self.name2ring[name].extralist:
							self.name2ring[name].extralist.remove(tb)
		for th in thqueue:
			th.join()

	def create_table(self,name='',tbname=''):
		self.modify_table('create',tbname,name)

	def create_table_on_all(self,tbname):
		tmpconfig=deepcopy(config)
		for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]
			tmpconfig['host']=ip
			cnx=mysql.connector.connect(**tmpconfig)
			cursor=cnx.cursor(dictionary=True)
			cmd=TABLES[tbname]
			cursor.execute(cmd)
			cursor.close()
			cnx.close()
			if PRINTLEVEL>=INFO_PRINT: print 'create %d table %s'%(nid,tbname)
		for na in self.name2ring.keys():
			if not tbname in self.name2ring[na].extralist:
				self.name2ring[na].extralist.append(tbname)

	def drop_table(self,name='',tbname=''):
		self.modify_table('drop',tbname,name)

	def drop_table_on_all(self,tbname):
		tmpconfig=deepcopy(config)
		for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]
			tmpconfig['host']=ip
			cnx=mysql.connector.connect(**tmpconfig)
			cursor=cnx.cursor(dictionary=True)
			cursor.execute("DROP TABLE IF EXISTS "+tbname)
			cursor.close()
			cnx.close()
			if PRINTLEVEL>=INFO_PRINT: print 'drop on %d table %s'%(nid,tbname)
		for na in self.name2ring.keys():
			if tbname in self.name2ring[na].extralist:
				self.name2ring[na].extralist.remove(tbname)

	def create_db(self,name=''): # name is category name, not dbname
		if name=='': # create db on all current nodes in all rings
			for nid in self.nid2connPool.keys():
				cnx= self.nid2connPool[nid].get_connection()
				cursor=cnx.cursor(dictionary=True)
				try:
					qry="CREATE DATABASE IF NOT EXISTS "+dbname
					if PRINTLEVEL>=DEBUG_PRINT: print qry
					cursor.execute(qry)
				except Exception, e:
					if PRINTLEVEL>=ERROR_PRINT: print(e)
				finally:
					cursor.close()
					cnx.close()
		else: # create db on nodes in specified ring
			for nid in self.name2ring[name].nid2node.keys():
				cnx= self.nid2connPool[nid].get_connection()
				cursor=cnx.cursor(dictionary=True)
				try:
					qry="CREATE DATABASE IF NOT EXISTS "+dbname
					if PRINTLEVEL>=DEBUG_PRINT: print qry
					cursor.execute(qry)
				except Exception, e:
					if PRINTLEVEL>=ERROR_PRINT: print(e)
				finally:
					cursor.close()
					cnx.close()
	
	def create_db_on_all(self,db=dbname): # preferred func
		tmpnodbconfig=deepcopy(nodbconfig)
		for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]			
			tmpnodbconfig['host']=ip
			cnx=mysql.connector.connect(**tmpnodbconfig)
			cursor=cnx.cursor(dictionary=True)
			cursor.execute("CREATE DATABASE IF NOT EXISTS "+db)
			cursor.close()
			cnx.close()
			if PRINTLEVEL>=INFO_PRINT: print 'create on %d db %s'%(nid,db)

	def drop_db(self,name=''): # name is category name, not dbname
		if name=='': # drop db on all current nodes in all rings, call create_db
			for nid in self.nid2connPool.keys():
				cnx= self.nid2connPool[nid].get_connection()
				cursor=cnx.cursor(dictionary=True)
				try:
					qry="DROP DATABASE IF EXISTS "+dbname
					if PRINTLEVEL>=DEBUG_PRINT: print qry
					cursor.execute(qry)
				except Exception, e:
					if PRINTLEVEL>=ERROR_PRINT: print(e)
				finally:
					cursor.close()
					cnx.close()
		else:  # drop db on nodes in ring name, call create_db immediately after
			for nid in self.name2ring[name].nid2node.keys():
				cnx= self.nid2connPool[nid].get_connection()
				cursor=cnx.cursor(dictionary=True)
				try:
					qry="DROP DATABASE IF EXISTS "+dbname
					if PRINTLEVEL>=DEBUG_PRINT: print qry
					cursor.execute(qry)
				except Exception, e:
					if PRINTLEVEL>=ERROR_PRINT: print(e)
				finally:
					cursor.close()
					cnx.close()

	def drop_db_on_all(self,db=dbname):
		tmpnodbconfig=deepcopy(nodbconfig)
		for nid in range(0,TotalNodeNum):
			ip=nid2ip[nid]
			tmpnodbconfig['host']=ip
			cnx=mysql.connector.connect(**tmpnodbconfig)
			cursor=cnx.cursor(dictionary=True)
			cursor.execute("DROP DATABASE IF EXISTS "+db)
			cursor.close()
			cnx.close()
			if PRINTLEVEL>=INFO_PRINT: print 'drop on %d db %s'%(nid,db)

	def check_alive(self,intv=CheckAliveIntv):
		t = threading.Thread(target=self.th_check_alive,args=(intv,))
		t.setDaemon(True)
		t.start()

	def th_check_alive(self,intv=CheckAliveIntv):
		while True:
			time.sleep(CheckAliveIntv)
			busy=0
			for name in self.name2ring.keys():
				ring=self.name2ring[name]
				if len(ring.extradict['fixthread'])>=1:
					if PRINTLEVEL>=INFO_PRINT: print name+' queued th ',ring.extradict['fixthread'][0].getName()
					busy=1
			if busy:
				if PRINTLEVEL>=INFO_PRINT: print 'ck busy fixing... '+threading.currentThread().getName()
			if PRINTLEVEL>=INFO_PRINT: print 'checking alive... '+threading.currentThread().getName()
			tmpnodbconfig=deepcopy(nodbconfig)
			for nid in self.nid2connPool.keys():
				checkhim=True
				for na,ring in self.name2ring.items():
					if nid in ring.extradict['dead']:
						if len(ring.extradict['fixthread'])>=1: # only if host ring is busy.
							checkhim=False
							break # still check those already dead but not fixed yet, unless busy
				if not checkhim:
					continue
				ip=nid2ip[nid]
				tmpnodbconfig['host']=ip
				cnx=None
				try:
					if PRINTLEVEL>=INFO_PRINT: print 'checking '+`nid`
					cnx=mysql.connector.connect(**tmpnodbconfig)
				# operational error handles too many connections; a host name could not be resolved; bad handshake; server is shutting down, communication errors
				# except (ImportError,errors.OperationalError,AttributeError): # multiple error here don't work...sad
				# 	if PRINTLEVEL>=INFO_PRINT: print 'check alive ignore exception'
				# 	pass
				except Exception,e: 
					if cnx==None:# true exception remain here.
						if PRINTLEVEL>=ERROR_PRINT: print `nid`+' fail !'
						for name in self.name2ring.keys():
							ring=self.name2ring[name]
							if nid in ring.nid2node.keys():
								if PRINTLEVEL>=WARN_PRINT: print 'fixing %s ring %d died'%(name,nid)
								t = threading.Thread(target=self.th_fix_deadnode,args=(name,nid))
								t.setDaemon(True)
								if len(ring.extradict['fixthread'])==0:
									t.start()
								else:
									print t.getName()+' is queued ~'
								# one ring, one node at a time, avoid chaos.
								ring.extradict['fixthread'].append(t)
								if not nid in self.deadlist:
									self.deadlist.append(nid)
								if not nid in ring.extradict['dead']: # could append else where
									ring.extradict['dead'].append(nid) # notify ops on ring, append to dead list
								print 'fix th list',ring.extradict['fixthread']
				finally:
					if cnx:
						cnx.close()

	def th_fix_deadnode(self,name,nid): # fix dead nid on ring name
		ring=self.name2ring[name]
		deadnode=ring.nid2node[nid]
		thqueueBAKNUM=[] # threads updating baknum cannot concurrent for each predecessor, or -1 chaos
		thqueTRANSFER=[] # threads transfering to new nodes can run in parallel
		for deadpos in deadnode.poslist:
			avoid1=[]
			if NumOfCopies<=1:
				if PRINTLEVEL>=ERROR_PRINT: print 'num of copies=1, no backup, dead...'
			predpos=deadpos
			for front in range(0,NumOfCopies): # transfer predecessor
				if front==0: # when primary backup is on dead node, deadpos' sucpos become primary copy, transfer to another new node. Then, decrease successor baknum. (must transfer before dec baknum at front=0!)
					for tbname in ring.extralist:
						if tbname=='tweets':
							sp=ring.find_succ_diffnode(deadpos,[]) # dont use avoid2, since it starts back from dead pos
							sucpos=deadpos
							avoid3=[]
							for behind in range(NumOfCopies):
								sucpos=ring.find_succ_diffnode(sucpos,avoid3)
							# use new avoid list, since it's from deadpos start
							fromnid=ring.pos2node[sp].nid
							tonid=ring.pos2node[sucpos].nid
							if PRINTLEVEL>=INFO_PRINT: print 'transfering from %d to %d'%(fromnid,tonid)
							self.th_transfer_table(fromnid,tonid,tbname,name,1,(NumOfCopies-1)) # must wait, cannot new thread here
							if PRINTLEVEL>=INFO_PRINT: print 'a transfer done at %s %d'%(name,nid)
				else: # don't modify predpos in front==0
					predpos=ring.find_pred_diffnode(predpos,avoid1) # find predecessor
				sucpos=deadpos
				avoid2=[]
				for behind in range(1,NumOfCopies-front):
					sucpos=ring.find_succ_diffnode(sucpos,avoid2)
					if sucpos==deadpos:
						continue
					# decrease baknum at sucpos
					for tbname in ring.extralist:
						if tbname=='tweets':
							t = threading.Thread(target=self.th_update_table,args=(ring.pos2node[sucpos].nid,name,tbname,{'baknum':[0,'-1']},{'category':name,'baknum':front+behind}))
							t.setDaemon(True)
							t.start()
							thqueueBAKNUM.append(t)
				if front!=0: # previouly, when dead is primary, already processed
					# cannot move baknum=0 from dead node to new node.
					sucpos=ring.find_succ_diffnode(sucpos,avoid2)
					# transfer from predpos (baknum=0) to sucpos (baknum=numofcopies-1)
					for tbname in ring.extralist:
						if tbname=='tweets':
							fromnid=ring.pos2node[predpos].nid
							tonid=ring.pos2node[sucpos].nid
							if PRINTLEVEL>=INFO_PRINT: print 'transfering from %d to %d'%(fromnid,tonid)
							t = threading.Thread(target=self.th_transfer_table,args=(fromnid,tonid,tbname,name,0,(NumOfCopies-1)))
							t.setDaemon(True)
							t.start()
							thqueTRANSFER.append(t)
				while len(thqueueBAKNUM)>0:
					th=thqueueBAKNUM.pop(0)
					th.join() # repair once per predecessor (in front loop), or chaos.
					if PRINTLEVEL>=INFO_PRINT: print 'a baknum dec done at %s %d'%(name,nid)
				
			while len(thqueTRANSFER)>0:
				th=thqueTRANSFER.pop(0)
				th.join() # transfer can run concurrent for different predecessor, but not deadpos.
				if PRINTLEVEL>=INFO_PRINT: print 'a transfer done at %s %d'%(name,nid)

		if PRINTLEVEL>=INFO_PRINT: print 'fixed ring '+name+' nid '+`nid`
		ring.extradict['fixthread'].remove(threading.currentThread())
		if nid in self.nid2connPool.keys():
			del self.nid2connPool[nid]
		ring.remove_node(nid) # first remove nid on ring
		while nid in ring.extradict['dead']: # thread unsafe, could be appended multiple times 
			ring.extradict['dead'].remove(nid) # 2nd remove from deadlist, or concurrent ops may go wrong
		while nid in self.deadlist:
			self.deadlist.remove(nid)
		if PRINTLEVEL>=INFO_PRINT:print 'nid '+`nid`+' removed from ',ring.extradict['dead']
		if len(ring.extradict['fixthread'])>0:
			th=ring.extradict['fixthread'][0] # cannot pop, if removed, len=0 deem not busy...
			th.start() # start queued fix thread, one at a time.
			print "previously queued th start as "+th.getName()

	def th_transfer_table(self,fromnid,tonid,tbname,name,baknum=0,setbaknum=(NumOfCopies-1)):
		if fromnid in self.deadlist or tonid in self.deadlist:
			return
		result=[]
		ring=self.name2ring[name]
		cmd="SELECT * FROM "+tbname+" WHERE baknum="+str(baknum)+" AND category='"+name+"'"
		self.th_exec_raw(fromnid,cmd,result,read=True,write=False)
		qry1=''
		qry3=')'
		for row in result:
			if qry1=='': # for efficiency, qry1 only process once, assuming attri stable.
				qry1='INSERT INTO '+tbname+'('
				for attri,val in row.items():
					qry1=qry1+attri+","
				if qry1.endswith(','):
					qry1=qry1[:-len(',')]
			qry2=') VALUES (' # qry2 changes every time so cannot help...
			for attri,val in row.items():
				if isinstance(val,str) or isinstance(val, unicode):
					qry2=qry2+"'"+val.replace('"', '\\"').replace("'", "\\'")+"',"
				elif attri=='timestamp':
					qry2=qry2+"'"+val.strftime("%Y-%m-%d %H:%M:%S")+"',"
				elif attri=='baknum':
					qry2=qry2+str(setbaknum)+","
				else:
					qry2=qry2+str(val)+","
			if qry2.endswith(','):
				qry2=qry2[:-len(',')]
			cmd=qry1+qry2+qry3
			self.th_exec_raw(tonid,cmd,[],read=False,write=True)

	def th_update_table(self,nid,name,tbname,changedict,wheredict): 
		#changedict {'baknum':[0, '-1'],'attr':[1, 'asdf']} 0: relative change, 1: set absolute val
		#wheredict {'category':name}
		if nid in self.deadlist:
			return
		qry1="UPDATE "+tbname+" SET "
		qry2=" WHERE "
		# where category = name
		for cond in wheredict.keys():
			if isinstance(wheredict[cond], str) or isinstance(wheredict[cond], unicode):
				qry2=qry2+cond+"='"+wheredict[cond]+"' AND "
			else:
				qry2=qry2+cond+"="+str(wheredict[cond])+" AND "
		# set baknum = baknum-1 
		for attri in changedict.keys():
			if changedict[attri][0]==0: # relative change
				qry1=qry1+attri+"="+attri+changedict[attri][1]+','
			else: # absolute change
				qry1=qry1+attri+"="+changedict[attri][1]+','
		if qry2.endswith('AND '):
			qry2=qry2[:-len('AND ')]
		if qry1.endswith(','):
			qry1=qry1[:-len(',')]
		cmd=qry1+qry2
		self.th_exec_raw(nid,cmd,[],read=False,write=True)


TABLES = {}

TABLES['user'] = (
    "CREATE TABLE IF NOT EXISTS `user` ("
    "  `uid` INT UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(20) NOT NULL default 'default',"
    "  `password` varchar(30) NOT NULL default '123456',"
    "  `email` varchar(40) NOT NULL UNIQUE,"
    "  `token` varchar(130) UNIQUE default NULL,"
    "  `baknum` INT UNSIGNED NOT NULL default 0,"
    "  PRIMARY KEY (`uid`)"
    ") ENGINE=InnoDB")
TABLES['author'] = (
    "CREATE TABLE IF NOT EXISTS `author` ("
    "  `aid` INT UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `name` varchar(20) NOT NULL default 'default',"
    "  `numfollowers` INT UNSIGNED DEFAULT 0,"
    "  `username` varchar(20) default '',"
    "  `email` varchar(40) default '',"
    "  `baknum` INT UNSIGNED NOT NULL default 0,"
    "  PRIMARY KEY (`aid`)"
    ") ENGINE=InnoDB")
TABLES['tweets'] = (
    "CREATE TABLE IF NOT EXISTS `tweets` ("
    "  `tid` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `timestamp` DATETIME NOT NULL DEFAULT '2035-01-01 23:59:59',"
    "  `url` varchar(100) default '',"
    "  `content` varchar(150) not null default '',"
    "  `category` varchar(50) not null,"
    "  `author` varchar(50),"
    "  `baknum` INT UNSIGNED NOT NULL default 0,"
    "  PRIMARY KEY (`tid`)"
    ") ENGINE=InnoDB")
TABLES['prefer'] = (
    "CREATE TABLE IF NOT EXISTS `prefer` ("
    "  `uid` INT UNSIGNED NOT NULL AUTO_INCREMENT,"
    "  `category` varchar(50) not null,"
    "  `keywords` varchar(20) not null,"
    "  `weight` real default 0,"
    "  `baknum` INT UNSIGNED NOT NULL default 0,"
    "  PRIMARY KEY (`uid`,`category`,`keywords`)"
    ") ENGINE=InnoDB")

init_name2nids={
'Sports':       [ 0,10,20,30,19, 7,15,23],
'Technology':   [ 2,12,22,32, 1, 9,17,25],
'Science':      [ 4,14,24,34, 3,11,19,27],
'Entertainment':[ 5,15,25, 8, 4,12,20,28],
'News':         [ 6,11,21,31, 0, 8,16,24],

# 'health':       [ 3,13,23,33, 2,10,18,26],
# 'world':        [ 1,16,26, 9, 5,13,21,29],
# 'business':     [ 7,17,27,18, 6,14,22,30],

}

if __name__ == "__main__":
	if PRINTLEVEL>=DEBUG_PRINT: print TABLES

#cnxpool=MySQLConnectionPool(pool_name=None,pool_size=5,**kwargs)
#cnxpool.add_connection(cnx) # add existing connection to pool,raises a PoolError if the pool is full
#cnxpool.set_config(**kwargs) # no use here
#cnx= cnxpool.get_connection()#raises a PoolError if no connections are available.
# name = cnxpool.pool_name

#SELECT daytime FROM tb WHERE daytime >= NOW() or >='2014-05-18 15:00:00'
# cursor.execute("SELECT * FROM books")
#         row = cursor.fetchone()# fetchone(),  fetchmany() or  fetchall() method to fetch data from the result set.
#         while row is not None:
#			 print(row)  #printed out the content of the row and move to the next row until all rows are fetched.
#             row = cursor.fetchone()

## if row number is small:
# cursor.execute("SELECT * FROM books")
#         rows = cursor.fetchall()
#         print('Total Row(s):', cursor.rowcount)
#         for row in rows:
#             print(row)

#fetchmany() method that returns the next number of rows (n) of the result set, which allows us to balance between time and memory space.
# rows = cursor.fetchmany(size)
#         if not rows:
#             break
#         for row in rows:
#             yield row