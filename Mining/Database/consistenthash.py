#!/usr/bin/env python

import os, sys
import subprocess, threading
import random, time
import platform
import inspect
mypydir =os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.append(mypydir)
import hashlib

from hostip import host2ip, ip2host, host2userip
import logging

defaultBITS=8
defaultVirtualNode=1 # if virtual node, then no backup (baknum doesn't work). if no virtual node (=1) then we can have backup copies. But more complicate per node controller can have both; centralized controller cannot have both.

def get_hash(num, bits=defaultBITS):
	BITS=bits
	SIZE=2**BITS # 256
	SIZEm1=SIZE-1  # 255
	if not isinstance(num,str): # convert to str
		num=str(num)
	md=hashlib.md5()
	md.update(num)
	h=md.hexdigest()
	d=int(h,16)
	#return int(d & SIZEm1) # simply use lower 8 bits, but hash result not good. tooclose
	res=0
	while d>0:
		res+=(d & SIZEm1)
		d=d>> BITS
	return int(res)% SIZE

class Node:
	def __init__(self,nodeid):
		self.nid=nodeid
		self.intvlist=[] # key intervals
		self.poslist=[] # its positions in ring
		self.idforhash=-1 # this is the real id used for hash, to avoid collision.
		self.pos2succpos={} # pos:successor pos, pos2 :spos2 ...posVirtualN: suc posVir
		self.pos2predpos={} # pos:predecessor pos, pos2 :ppos2 ...posVirtualN: pred posVir

class Ring:
	def __init__(self,bit=defaultBITS,vir=defaultVirtualNode):
		self.lock=threading.Lock()
		self.BITS=bit
		self.SIZE=2**self.BITS # 256
		self.SIZEm1=self.SIZE-1  # 255
		self.pos2node={} # pos:node
		self.virtualNum=vir # num of virtual nodes for each physical node.
		self.nid2node={}
		self.sortedpos=[]
		self.extralist=[] # used to store some info
		self.extradict={'state':0,'fixthread':[],'dead':[],}
		#self.snapPos2nid={} # only for snapshot, failure detection use.

	def find_succ_diffnode(self,pos,avoidlist=[]):# find successor that's not himself
		mynid=self.pos2node[pos].nid
		sucpos=self.pos2node[pos].pos2succpos[pos]
		deadloop=0
		avoidlist.append(mynid)
		while self.pos2node[sucpos].nid in avoidlist:# might be the same node
			sucpos=self.nid2node[self.pos2node[sucpos].nid].pos2succpos[sucpos]
			deadloop+=1
			if deadloop>self.get_size():
				print 'error in finding successor ! deadloop'
				return -1
		avoidlist.append(self.pos2node[sucpos].nid)
		return sucpos 

	def find_pred_diffnode(self,pos,avoidlist=[]):# find predecessor that's not himself
		mynid=self.pos2node[pos].nid
		ppos=self.pos2node[pos].pos2predpos[pos]
		deadloop=0
		avoidlist.append(mynid)
		while self.pos2node[ppos].nid in avoidlist:# might be the same node
			ppos=self.nid2node[self.pos2node[ppos].nid].pos2predpos[ppos]
			deadloop+=1
			if deadloop>self.get_size():
				print 'error in finding predecessor ! deadloop'
				return -1
		avoidlist.append(self.pos2node[ppos].nid)
		return ppos 

	def key2nid(self,num): # given key id, which nid stores it?
		pos=get_hash(num,self.BITS)
		sz=len(self.sortedpos)
		i=0
		while i < (sz):
			if pos<=self.sortedpos[i]:
				break
			i+=1
		if i==sz:
			return self.pos2node[self.sortedpos[0]].nid
		else:
			return self.pos2node[self.sortedpos[i]].nid

	def key2pos(self,num): # given key id, which pos stores it?
		pos=get_hash(num,self.BITS)
		sz=len(self.sortedpos)
		i=0
		while i < (sz):
			if pos<=self.sortedpos[i]:
				break
			i+=1
		if i==sz:
			return self.sortedpos[0]
		else:
			return self.sortedpos[i]

	def add_node(self,nid):
		if len(self.pos2node)>0.8*self.SIZE:
			print "Ring Full! cannot add node."
			return 0
		nid=int(nid)
		self.lock.acquire() # lock from here, safer
		if self.get_nodeNum()>0:
			maxnid=max(self.nid2node.keys())
			if nid<maxnid:
				print "Ring add wrong nid, must ascending order!"
				return 0
		if nid in self.nid2node.keys():
			return 2
		nd=Node( nid )
		tmpid=nid
		success=False
		while not success:
			success=True
			poslist=[]
			for i in range(self.virtualNum): # put all virtual nodes
				pos=get_hash(tmpid+i,self.BITS)
				if pos in self.pos2node.keys():
					success=False
					tmpid+=1
					break
				else:
					poslist.append(pos)
		for p in sorted(poslist):
			self.pos2node[p]=nd
			nd.poslist.append(p)
		nd.idforhash=tmpid
		self.nid2node[nid]=(nd)
		self.update_successorpos()
		self.update_intvlist()
		self.lock.release()  # release lock after all done.
		return 1

	def remove_node(self, nid):
		if nid in self.nid2node.keys():
			nd=self.nid2node[nid]
			self.lock.acquire() # lock
			for pos in nd.poslist:
				del self.pos2node[pos]
			del self.nid2node[nid]
			self.update_successorpos()
			self.update_intvlist()
			self.lock.release()
			return True
		print `nid`+"'th node not exist. remove_node fail"
		return False

	def update_successorpos(self): # no need to lock, only changing individual node
		if self.get_size()==0:
			return
		self.sortedpos=sorted(self.pos2node.keys())
		sortpos=self.sortedpos
		sz=len(sortpos)
		for i in range(sz-1):
			self.pos2node[sortpos[i]].pos2succpos[sortpos[i]]=sortpos[i+1]
		self.pos2node[sortpos[sz-1]].pos2succpos[sortpos[sz-1]]=sortpos[0]
		for i in range(1,sz):
			self.pos2node[sortpos[i]].pos2predpos[sortpos[i]]=sortpos[i-1]
		self.pos2node[sortpos[0]].pos2predpos[sortpos[0]]=sortpos[sz-1]

	def update_intvlist(self):
		if self.get_size()==0:
			return
		for key in self.pos2node.keys():
			self.pos2node[key].intvlist=[] # clear old interval list
		self.sortedpos=sorted(self.pos2node.keys())
		sortpos=self.sortedpos
		sz=len(sortpos)
		for i in range(1,sz): 
			nd=self.pos2node[sortpos[i]]
			nd.intvlist.append([ sortpos[i-1], sortpos[i] ]) # (,]
		nd=self.pos2node[sortpos[0]]
		nd.intvlist.append([ sortpos[sz-1], sortpos[0] ]) # e.g. (255, 1] stores 0,1?
		nd.intvlist.append([ sortpos[sz-1]-256, sortpos[0] ]) # e.g. (-1, 1] stores 0,1
		nd.intvlist.append([ sortpos[sz-1], sortpos[0]+256 ]) # e.g. (255, 257] stores nothing

	def get_size(self):
		return len(self.pos2node)
	def get_nodeNum(self):
		return len(self.nid2node)
	def get_maxsize(self):
		return self.SIZE

if __name__ == "__main__":
	if len(sys.argv)>1:
		print get_hash(sys.argv[1],defaultBITS)

	r1=Ring()
	
	r1.add_node(2)
	r1.add_node(32)
	r1.add_node(10)
	r1.add_node(20)
	r1.add_node(121)
	r1.add_node(130)
	print r1.get_size()
	print r1.nid2node
	print r1.sortedpos
	r1.update_intvlist()
	r1.update_successorpos()
	for key in r1.pos2node.keys():
		node=r1.pos2node[key]
		print node.nid
		print node.poslist
		print node.intvlist
		print node.pos2succpos

	r1.remove_node(20)
	r1.remove_node(121)
	r1.remove_node(32)
	r1.update_successorpos()
	r1.update_intvlist()

	print r1.nid2node
	print r1.sortedpos
	for key in r1.pos2node.keys():
		node=r1.pos2node[key]
		print node.nid
		print node.poslist
		print node.intvlist
		print node.pos2succpos