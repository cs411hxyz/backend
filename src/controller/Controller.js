const restify = require('restify');

// Defines a prototypical base class called Controller
function Controller() {};

/*  class methods and variables  */
Controller.prototype = {
}

/*  static helper methods  */

// This function creates a new subclass
// It returns a BUILDER that contains following members:
//  1.setPathPrefix
//  2.addPUT, addGET, addPOST, addDELETE
Controller.create = (subclass) => {
  subclass.prototype = Object.create(Controller.prototype);
  subclass.isController = true;

  /*
   * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   * WARNING: DON'T use lambda (arrow functions) as argument
   *    WHEN YOU ARE using Controller.add* !!!
   * 
   *  BECAUSE: arrow functions' this is determined by
   *    where it is DEFINED, NOT where it is USED!
   * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   */
  var builder = {
    // This function defines prefix of API path for a subclass
    setPathPrefix : (prefix) => {
      subclass.prototype['RESTPathPrefix'] = prefix;
      return builder;
    },

    // Adds a PUT listener in the subclass given
    addPUT : (path, funcName, func) => {
      var proto = subclass.prototype;
      proto[funcName] = func;
      proto[funcName].isREST = true;
      proto[funcName].RESTMethod = "PUT";
      proto[funcName].RESTPath = path;
      return builder;
    },

    // Adds a POST listener in the subclass given
    addPOST : (path, funcName, func) => {
      var proto = subclass.prototype;
      proto[funcName] = func;
      proto[funcName].isREST = true;
      proto[funcName].RESTMethod = "POST";
      proto[funcName].RESTPath = path;
      return builder;
    },

    // Adds a GET listener in the subclass given
    addGET : (path, funcName, func) => {
      var proto = subclass.prototype;
      proto[funcName] = func;
      proto[funcName].isREST = true;
      proto[funcName].RESTMethod = "GET";
      proto[funcName].RESTPath = path;
      return builder;
    },

    // Adds a DELETE listener in the subclass given
    addDELETE : (path, funcName, func) => {
      var proto = subclass.prototype;
      proto[funcName] = func;
      proto[funcName].isREST = true;
      proto[funcName].RESTMethod = "DELETE";
      proto[funcName].RESTPath = path;
      return builder;
    }
  };
  return builder;
};


// Used to load an object of subclass of Controller to RESTIFY
Controller.loadToServer = (object, server, version) => {
  prototype = Object.getPrototypeOf(object);
  for(var fieldName in prototype) if(prototype.hasOwnProperty(fieldName))
    if(prototype[fieldName] instanceof Function) {
      var func = prototype[fieldName];
      if(!func.isREST)
        continue;
      if(typeof func.RESTMethod != "string")
        continue;
      var path = func.RESTPath;
      if(prototype.RESTPathPrefix)
        path = prototype.RESTPathPrefix + path;
      
      // LOG: what functionalities are at what directories
      console.log(fieldName, '@', func.RESTMethod, path);

      switch(func.RESTMethod) {
        case "PUT": server.put({
          path: path,
          version: version
        }, func.bind(object)); break;
        case "GET": server.get({
          path: path,
          version: version
        }, func.bind(object)); break;
        case "POST": server.post({
          path: path,
          version: version
        }, func.bind(object)); break;
        case "DELETE": server.del({
          path: path,
          version: version
        }, func.bind(object)); break;
        default: console.log("Unexpected error at Controller.js");
      }
    }
};

module.exports = Controller;
