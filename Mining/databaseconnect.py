import sys, os, inspect
def database_connect():
	print "Connecting to Database"
	## Connect to Database
	mypydir =os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
	sys.path.append(mypydir+'/Database/')
	import database
	categories = ['News', 'Sports', 'Technology', 'Entertainment', 'Science']
	db=database.Database()
	for category in categories:
		db.createCategory(category)
		db.create_table(category,'tweets')
	db.check_alive(3)
	##
	print "Connected to Database"
	return db