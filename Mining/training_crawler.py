import tweepy
import os
import time

cwd = os.getcwd()
auth = tweepy.OAuthHandler('KdD8E83BaN1qjomANuHopyfST', 'ma5wTyB0nJswDWL54pcI46CFGU8gZOuslxMR0gDxtn64dTyGpu')
auth.set_access_token('3858849075-EYJBgdCrknP7RZZNabRv2OzVsp9R58SLTgIpMUR', 'rTRBkrxksqaXmKrmeRLxXqskgofdKlcx8QqEBsf824AeZ')
# Create the tweepy API object
api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True,retry_count=3,retry_delay=5)

training_accounts = open('training_accounts.txt').readlines()
for category in training_accounts:
    category_name = category.split(':')[0]
    category_accounts = category.split(':')[1].replace(' ','').replace('\n','').split(',')
    for account in category_accounts:
        tweets = tweepy.Cursor(api.user_timeline, screen_name=account).items()
        #tweets = api.user_timeline(screen_name=account,count=200) # For debugging
        i = 0
        for tweet in tweets:
            try:
                i += 1
                with open(cwd+'/Training_Data/'+category_name,'a') as text_output:
                    text_output.write(tweet.text.encode('ascii', 'ignore').replace('\n','').replace('\t','')+'\n')
                with open(cwd+'/Training_Data/'+category_name+'_meta','a') as text_output:
                    text_output.write(account+'\n')
            except:
                print 'Exception: '+account+' '+str(i)
        print account+': '+str(i)
        print 'Sleeping for 15 minutes'
        time.sleep(900) # Sleep for 15 minutes to avoid being rate limited by Twitter