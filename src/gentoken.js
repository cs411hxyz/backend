var uuid = require('node-uuid');

// This module should only help generate a token.
//   GetCurrentUser should be a method injected elsewhere, by AuthController.authInject

module.exports = () => {
  return Date.now() + '-' + uuid.v4();
};
