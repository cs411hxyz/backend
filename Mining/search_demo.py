import requests
import json
# Before trying this, make sure searchinterface.py is running (it might take upto 2 minutes to connect to the database initially)
# data is the query passed to the webserver as a JSON object.
# If you want to search all categories send '' as the content of category
# The search engine will return all tweets AFTER the given time (make sure to use the same format as below)
# 'feedback' controls whether this query contains feedback tweets or not
# 'feedback_tweets' should contains a list of the tweets that user have selected as relevant in the format below
# Currently the author information (Name and number of followers) are not included with the tweets. I will these soon.
data = {'query': 'paris', 'category':'News','time':'2015-10-01 23:59:59','feedback':'False','feedback_tweets':"['first tweet','second tweet']",'min_num_followers':'10'}
response = requests.post('http://127.0.0.1:8899/search', json=data)
tweets = json.loads(response.content)
for tweet in tweets:
	# Each tweet is a JSON object which has several atrributes
    print tweet