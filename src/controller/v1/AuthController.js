var gentoken = require('../../gentoken');
var crypto = require('crypto');
// loadDB() is a promise. If already fulfilled <=> valid connection returned.
var loadDB = require('../../dbconn');
var restify = require('restify');
var CtrlBuilder = require('../Controller');
var HttpStatus = require('http-status-codes');

// Defines a prototypical class called AuthController
function AuthController() {}

AuthController.authInject = (req, res, next) => {
  
  // A function that handles the case where
  //   the user did not provide a valid auth-token
  function noAuthToken(reason) {
    var path = req.getPath();
    if(/^\/*auth\/+(login|signup)$/.test(path) || /^\/*test($|\/+)/.test(path))
    // Anonymouse access only allowed for /auth/login, /auth/signup, and /test/*
      return next();
    else {
      reason = reason ? ":\n" + reason.toString() : ".";
      res.send(HttpStatus.FORBIDDEN, { error: "Please login first" + reason});
      return;
    }
  }

  // Check whether the params contains token at all.
  if(!req.params["auth-token"])
    return noAuthToken("Authentication information is not provided.");

  // Otherwise, check whether the token is valid HERE.
  var sql;
  loadDB().then(conn => {
    sql = conn;
    var qryStr = sql.format(
      'SELECT uid, name FROM user WHERE token = ?',
      [req.params["auth-token"]]
    );
    return sql.query(qryStr);
  }).then(rows => {
    if(rows.length!==1)
      throw 'You have been logged out. Invalid Token. Please Login Again.';
    // Otherwise, INJECT uid and name in the 'req' object for other controllers to use.
    req.userid = rows[0].uid;
    req.username = rows[0].name;

    // Disconnect from sql
    sql.destroy();

    // Use try-catch to prevent err in next() to cause a noAuthToken call.
    try {next();} catch(err) {console.log(err.toString());}
  }).catch(err => {
    noAuthToken(err);
    sql.destroy();
  });
};

CtrlBuilder.create(AuthController)
  .setPathPrefix('/auth')

  /**************** SIGN UP *******************/
  .addPOST('/signup', 'signup', function(request, response, next) {
    var username = request.params.username;
    var password = request.params.password;
    var dryrun = request.params.dryrun;

    if(!username || !password)
    {
      response.send(HttpStatus.BAD_REQUEST, {error: 'username and password cannot be empty'});
      return;
    }

    var sql;
    var token;
    loadDB().then(conn => {
      sql = conn;
      return sql.query('SELECT uid FROM user WHERE name = ' + sql.escape(username));
    }).then(rows => {
      // determine whether the user has already registered.
      if(rows.length!==0)
        throw 'This username has already been registered, please try again!';
    }).then(() => {
      if(dryrun === "false" || dryrun === false)
      {
        token = gentoken();
        var qryStr = sql.format(
          'INSERT INTO user (name, password, token) VALUES (?, ?, ?);',
          [username, password, token]
        );
        sql.query(qryStr);
      }
    }).then(() => {
      sql.end();
      response.send(HttpStatus.OK, {'auth-token': token});
    }).catch(err => {
      response.send(HttpStatus.FORBIDDEN, {error: err.toString()});
      sql.destroy();
    });
  })

  /**************** LOGIN *******************/
  .addPOST('/login', 'login', function(request, response, next) {
    var username = request.params.username;
    var password = request.params.password;

    if(!username || !password)
    {
      response.send(HttpStatus.BAD_REQUEST, {error: 'username and password cannot be empty'});
      return;
    }

    var sql;
    loadDB().then(conn => {
      sql = conn;
      var sqlStr = sql.format(
        'SELECT token FROM user WHERE name = ? AND password = ?',
        [username, password]
      );
      return sql.query(sqlStr);
    }).then(rows => {
      // determine whether the user has already registered.
      if(rows.length!==1)
        throw 'Incorrect username or password!';
      response.send(HttpStatus.OK, {'auth-token': rows[0].token});
      sql.destroy();
    }).catch(err => {
      response.send(HttpStatus.FORBIDDEN, {error: err.toString()});
      sql.destroy();
    });
  });

module.exports = AuthController;
