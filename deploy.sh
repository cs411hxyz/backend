#!/bin/bash
pgrep CS411Backend
if [ $? == 0 ];
then
	echo -n "Server running. Kill? (y/*)"
	read -n 1 choice
	echo
	if [ $choice == "y" ];
	then
		pkill -TERM CS411Backend
		if [ $? != 0 ];
		then
			echo "Failed to kill server"
			exit 1
		fi
		pgrep CS411Backend
		if [ $? == 0 ];
		then
			echo "Failed to kill server"
			exit 1
		fi
	else
		exit 0
	fi
fi
echo -n "git pull? (y/*)"
read -n 1 choice
echo
if [ $choice == "y" ];
then
	git pull origin master
fi
(npm start)&
