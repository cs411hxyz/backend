#!/usr/bin/env python

import os, sys, threading
import subprocess
import random, time

import inspect
mypydir =os.path.abspath(os.path.dirname(inspect.getfile(inspect.currentframe())))
sys.path.append(mypydir)

import database

if __name__ == "__main__":
	if len(sys.argv)>1:
		extra=sys.argv[1]
	else:
		extra=''
	
	db=database.Database() # only one instance for all category

	if extra=='dropdb':  # if you want to restart on all nodes. Careful!
		db.drop_db_on_all()
		db.create_db_on_all()
		sys.exit(0)

	if extra=='droptb':
		t = threading.Thread(target=db.drop_table_on_all,args=('tweets',))
		t.setDaemon(True)
		t.start()
		t2 = threading.Thread(target=db.drop_table_on_all,args=('author',))
		t2.setDaemon(True)
		t2.start()
		t.join()
		t2.join()
		t = threading.Thread(target=db.create_table_on_all,args=('author',))
		t.setDaemon(True)
		t.start()
		t2 = threading.Thread(target=db.create_table_on_all,args=('tweets',))
		t2.setDaemon(True)
		t2.start()
		t.join()
		t2.join()
		sys.exit(0)
	
	category='Science'
	# category='Sports'
	db.createCategory(category) # this creates a ring of nodes to store catetory
	db.create_table(category,'tweets')  # even if table already there, still need call this func
# call db.create_table(category,table-name) every time you create a ring of nodes! 
#however, 'author' don't need category or ring, it is on all 35 nodes

	#db.check_alive(3)

	nextid=db.insert({'table':'tweets',
		'content':'1', # we can handle special char like '\''
		'category':category , # category is mandatory
		'timestamp':'2015-01-03 23:59:00' , # time exactly this format, 
		'url':'http://www.twitter.com' , # url is not mandatory
		'author':'zyr',

		})
	for i in range(1):
		nextid=db.insert({'table':'tweets',
			'id':nextid,
			'content':str(nextid), # we can handle special char like '\''
			'category':category , # category is mandatory
			})

	nextid=db.insert({'table':'author', # author on all nodes
		'name':'Yiran Zhao',  # no need to specify catefory
		'username':'zyrtwitter22',
		'email':'zyr22@gmail.com' ,
		'numfollowers':1020,
		})

	result=db.fetch({'table':'tweets',
		'category':category ,
		'keywords':['3','1'],
		'id':'',
		'author':'', 
		})# [{'content': u"this alan's example!\n'", 'id': 1}, ]
	print result,'\n\n'

# to fetch all category, you need to create ring for all categories first
	category='Science'
	db.createCategory(category)
	category='Sports'
	db.createCategory(category) # you also want fetch sports.

	result=db.fetch({'table':'tweets', # table mandotory
		'timestamp':'2015-01-01 00:00:00' , # later than this timestamp will be selected
		'category':'', # science and sports
		'author':'zyr',
		})
	print result,'\n\n'

	result=db.fetch({'table':'author',
		'name':'', # return specified name
		'email':'',
		'numfollowers':'',
		})
	print result

# select tweets where author has >= num of followers:
	ret1={'content':'','category':'Science','timestamp':'2015-01-01 00:00:00','id':'','keywords':['1','2']}
	ret2={'id':'','numfollowers':10,'email':'',"name":''}
	join={"tweets":"author","author":"name"} # common attributes, tb:attri
# select content,category,tid,aid,numfollowers,email 
# from tweets inner join author
# on tweets.author=author.name
# where timestamp>='2015-01-01 00:00:00' and numfollowers>=10
	result=db.select_from_join("tweets",ret1,"author",ret2,join)
	print result

	while True:
		try:
			s=raw_input('Control-C to exit')
		except KeyboardInterrupt:
			sys.exit(0)



######### my sys debug ######### end of tutorial
	sys.exit(0)
	ring=db.name2ring['catezyr']
	cmd="select tid,baknum from tweets"
	for nid in ring.nid2node.keys():
		result=[]
		db.th_exec_raw(nid,cmd,result,read=True,write=False)
		print '\nnid='+str(nid)
		for row in result:
			print row 
	
	print 'pos2node\n',ring.pos2node
	print 'nid2node\n',ring.nid2node
	print 'sortedpos\n',ring.sortedpos
	print 'extralist\n',ring.extralist
	print 'extradict\n',ring.extradict

	while True:
		try:
			s=raw_input('Control-C to exit')
			if s.startswith('f '):
				tid=int(s.split()[1])
				result=db.fetch({'table':'tweets',
					'category':'catezyr' ,
					'id':tid,
					'baknum':'',
					})
			if s.startswith('f'):
				result=db.fetch({'table':'tweets',
					'category':'catezyr' ,
					'id':'',
					'baknum':'',
					})
			
			for row in result:
				print row 

		except KeyboardInterrupt:
			sys.exit(0)

	sys.exit(0)




