var restify = require('restify');
var requireDir = require('require-dir');
var Controller = require('./controller/Controller');
var AuthModule = require('./controller/v1/AuthController');

var server = restify.createServer({
  name: 'cs411backend',
  version: '1.0.0'
});
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

// Allow CORS, MUST be used BEFORE AuthModule.authInject
server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

server.pre(restify.pre.sanitizePath());
server.use(AuthModule.authInject);

var v1 = requireDir('./controller/v1', { recurse: false });
for(var v1key in v1) if(v1.hasOwnProperty(v1key))
{
  var controller = v1[v1key];
  if(controller.isController)
    Controller.loadToServer(new controller(), server, '1.0.0');
}

server.listen(process.env.PORT || 8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});

// -- Process Management related code --
process.title = "CS411Backend";   // kill server by 'pkill CS411Backend'
process.on('SIGTERM', function() {
  console.log('Got SIGTERM.  Server Exiting...');
  process.exit();
});
