var loadDB = require('./dbconn');

var sql;
loadDB().then(conn => {
  sql = conn;
  try {
    sql.query("DROP TABLE IF EXISTS user;");
  } catch(err) {
    console.log('Error when dropping table tweets:', err);
  }
  sql.query('CREATE TABLE user ('       +
    'uid  int(10) unsigned  NOT NULL '  +
    '   PRIMARY KEY AUTO_INCREMENT  ,'  +
    'name varchar(20)       NOT NULL,'  +
    'password varchar(30)   NOT NULL,'  +
    'token varchar(130)     UNIQUE );'
    );
}).then(() => {
  console.log("Success!\n");
  sql.end();
});
