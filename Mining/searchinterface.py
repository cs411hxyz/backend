import cherrypy
import json
import datetime
import inspect, os, sys
from searchcore import search_interface, connect

f = open('searchlog.txt','a')
class Root(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self):
        return {'key1': 'value1'},{'key2': 'value2'}   
    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def search(self): # ,query, category, time, feedback, feedback_tweets
        data = cherrypy.request.json
        query = data["query"]
        category = data["category"]
        time = data["time"]
        feedback = data["feedback"]
        feedback_tweets = data["feedback_tweets"]
        num_followers = int(data["min_num_followers"])
        print query
        f.write(str(data)+'\n')
        #return str(type(query))
        #return {'key1': 'value1'},{'key2': 'value2'}
        return search_interface(query, category, time, feedback=='True', feedback_tweets, num_followers, False)

connect() # Connects to the DB
print "Starting Web interface !\n"
cherrypy.quickstart(Root(), config = "cherrypy.config") # Runs the internal webserver
f.close()
